zig: main.go
	go fmt
	go build

zip: zig main.go Makefile
	./zig test.zip main.go Makefile
