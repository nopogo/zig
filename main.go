package main

// zig # Zip in Go

import (
	"archive/zip"
	"io"
	"log"
	"os"
	"path/filepath"
)

func main() {
	zip_name := os.Args[1]
	output, err := os.OpenFile(zip_name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer output.Close()
	z := zip.NewWriter(output)
	defer z.Close()

	add_file := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Println(err)
			return nil
		}
		if info.IsDir() {
			return nil
		}
		if path == zip_name {
			log.Println("SKIP:", path)
			return nil
		}

		// add file to zip

		fh, err := zip.FileInfoHeader(info)
		if err != nil {
			log.Println(err)
			return nil
		}

		fh.Name = path
		fh.Method = zip.Deflate

		r, err := os.Open(path)
		if err != nil {
			log.Println(err)
			return nil
		}
		defer r.Close()

		w, err := z.CreateHeader(fh)
		if err != nil {
			log.Fatal(err)
		}

		_, err = io.Copy(w, r)
		if err != nil {
			log.Fatal(err)
		}
		return nil
	}

	for _, name := range os.Args[2:] {
		err = filepath.Walk(name, add_file)
	}
}
